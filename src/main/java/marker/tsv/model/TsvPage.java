package marker.tsv.model;

import java.util.List;

public class TsvPage {

    private int pageNo;
    private List<TsvRecord> records;
    private boolean last;

    public TsvPage(List<TsvRecord> records, int pageNo) {
        this.records = records;
        this.pageNo = pageNo;
    }

    public List<TsvRecord> getRecords() {
        return records;
    }

    public boolean isFirst() {
        return pageNo == 1;
    }

    public boolean isLast() {
        return last;
    }

    public void markAsLast() {
        last = true;
    }

    // For test purposes
    public void print() {
        System.out.println("====Page No: " + pageNo + " ====");
        for (TsvRecord record : records) {
            System.out.println(record);
        }
    }
}
