package marker.tsv.model;

import org.apache.commons.csv.CSVRecord;

public class TsvRecord {

    public static class Header {
        public static String SENTENCE = "Sentence";
        public static String HEADER_FOOTER = "IsHeaderFooter";
        public static String PAGE = "Page";
        public static String LINE = "Line";
        public static String X1 = "X1";
        public static String X2 = "X2";
        public static String Y = "Y";
        public static String PADDING = "Padding";
        public static String FONT = "Font";
    }

    public static int SENTENCE = 0;
    public static int HEADER_FOOTER = 1;
    public static int PAGE = 2;
    public static int LINE = 3;
    public static int X1 = 4;
    public static int X2 = 5;
    public static int Y = 6;
    public static int PADDING = 7;
    public static int FONT = 8;


    public TsvRecord(CSVRecord csvRecord) {
        sentence = csvRecord.get(SENTENCE);
        // ignore existing entries from test files
        // headerFooter = Boolean.valueOf(csvRecord.get(HEADER_FOOTER));
        page = Integer.parseInt(csvRecord.get(PAGE));
        line = Integer.parseInt(csvRecord.get(LINE));
        x1 = csvRecord.get(X1);
        x2 = csvRecord.get(X2);
        y = csvRecord.get(Y);
        padding = csvRecord.get(PADDING);
        font = csvRecord.get(FONT);
    }

    private String sentence;
    private boolean headerFooter;
    private int page;
    private int line;
    private String x1;
    private String x2;
    private String y;
    private String padding;
    private String font;

    public String getSentence() {
        return sentence;
    }

    public boolean isHeaderFooter() {
        return headerFooter;
    }

    public void setHeaderFooter(boolean headerFooter) {
        this.headerFooter = headerFooter;
    }

    public int getPage() {
        return page;
    }

    public int getLine() {
        return line;
    }

    public String getX1() {
        return x1;
    }

    public String getX2() {
        return x2;
    }

    public String getY() {
        return y;
    }

    public String getPadding() {
        return padding;
    }

    public String getFont() {
        return font;
    }

    @Override
    public String toString() {
        return "Page: " + page + " Line: " + line + " isHF: " + headerFooter + " Sentence: " + sentence;
    }
}
