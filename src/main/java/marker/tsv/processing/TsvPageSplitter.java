package marker.tsv.processing;

import marker.tsv.model.TsvPage;
import marker.tsv.model.TsvRecord;

import java.util.*;
import java.util.stream.Collectors;

public class TsvPageSplitter {
    public static final List<String> EXCLUSIONS = Arrays.asList("_HORIZONTAL_LINE");
    private static final int EX_PAGE_NO = 0;

    public List<TsvPage> split(List<TsvRecord> records) {
        List<TsvPage> pages = new ArrayList<>();

        Map<Integer, List<TsvRecord>> pagesMap =
                records.stream().collect(Collectors.
                        groupingBy(r -> EXCLUSIONS.contains(r.getSentence()) ? EX_PAGE_NO : r.getPage()));

        if (!pagesMap.containsKey(EX_PAGE_NO)) {
            pagesMap.put(EX_PAGE_NO, Collections.emptyList());
        }

        for (Integer pageNo : pagesMap.keySet()) {
            if (pageNo == EX_PAGE_NO)
                continue;
            pages.add(new TsvPage(pagesMap.get(pageNo), pageNo));
        }

        pages.get(pages.size() - 1).markAsLast();
        pages.add(new TsvPage(pagesMap.get(EX_PAGE_NO), EX_PAGE_NO));
        return pages;
    }
}
