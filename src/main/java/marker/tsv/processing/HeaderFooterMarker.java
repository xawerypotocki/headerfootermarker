package marker.tsv.processing;

import marker.tsv.model.TsvPage;
import marker.tsv.model.TsvRecord;

import java.util.List;

public class HeaderFooterMarker {
    public static final int DISTANCE_THRESHOLD = 3;

    private TsvPage currentPage;

    public void mark(List<TsvPage> pages) {
        for (int pageNo = 0; pageNo < pages.size() - 1; ++pageNo) {
            currentPage = pages.get(pageNo);
            if (!currentPage.isLast())
                compareWith(pages.get(pageNo + 1));
            if (!currentPage.isFirst())
                compareWith(pages.get(pageNo - 1));
        }
    }

    private void compareWith(TsvPage tsvPage) {
        doCompareWith(tsvPage, false);
        doCompareWith(tsvPage, true);
    }

    private void doCompareWith(TsvPage tsvPage, boolean endOfPage) {
        List<TsvRecord> recordsOfAnotherPage = tsvPage.getRecords();
        List<TsvRecord> recordsOfCurrentPage = currentPage.getRecords();
        for (int recordNo = 0;
             recordNo < recordsOfCurrentPage.size() && recordNo < recordsOfAnotherPage.size(); ++recordNo) {
            TsvRecord currentRecord =
                    recordsOfCurrentPage.get(endOfPage ? recordsOfCurrentPage.size() - 1 - recordNo : recordNo);
            TsvRecord anotherRecord =
                    recordsOfAnotherPage.get(endOfPage ? recordsOfAnotherPage.size() - 1 - recordNo : recordNo);

            if (compareSentences(currentRecord.getSentence(), anotherRecord.getSentence())) {
                currentRecord.setHeaderFooter(true);
            } else {
                break;
            }
        }
    }

    private boolean compareSentences(String sentence1, String sentence2) {
        return getDistance(sentence1, sentence2) < DISTANCE_THRESHOLD;
    }

    /**
     * Calculate Hamming Distance between str1 and str2.
     *
     * @return Hamming distance
     */
    private int getDistance(String str1, String str2) {
        str1 = str1.trim();
        str2 = str2.trim();
        if (str1.length() == str2.length()) {
            int distance = 0;
            for (int i = 0; i < str1.length(); i++) {
                if (str1.charAt(i) != str2.charAt(i)) {
                    distance++;
                }
            }
            return distance;
        } else {
            return Integer.MAX_VALUE;
        }
    }
}
