package marker.tsv.processing;

import marker.tsv.model.TsvRecord;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class TsvReader {

    public List<TsvRecord> read(String fileName) throws IOException {
        List<TsvRecord> records = new ArrayList<>();
        try (
                Reader reader = Files.newBufferedReader(Paths.get(fileName));
                CSVParser csvParser = new CSVParser(reader, CSVFormat.TDF.withFirstRecordAsHeader())
        ) {
            for (CSVRecord csvRecord : csvParser) {
                records.add(new TsvRecord(csvRecord));
            }
        }
        return records;
    }
}