package marker.tsv.processing;

import marker.tsv.model.TsvPage;
import marker.tsv.model.TsvRecord;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class TsvWriter {
    public void write(List<TsvPage> pages, String fileName) {

        try (
                Writer writer = Files.newBufferedWriter(Paths.get(fileName), StandardOpenOption.CREATE);
                CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.TDF)
        ) {
            writeHeader(csvPrinter);
            for (TsvPage page : pages) {
                for (TsvRecord record : page.getRecords()) {
                    csvPrinter.printRecord(record.getSentence(),
                            record.isHeaderFooter(),
                            record.getPage(),
                            record.getLine(),
                            record.getX1(),
                            record.getX2(),
                            record.getY(),
                            record.getPadding(),
                            record.getFont());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeHeader(CSVPrinter csvPrinter) throws IOException {
        csvPrinter.printRecord(TsvRecord.Header.SENTENCE,
                TsvRecord.Header.HEADER_FOOTER,
                TsvRecord.Header.PAGE,
                TsvRecord.Header.LINE,
                TsvRecord.Header.X1,
                TsvRecord.Header.X2,
                TsvRecord.Header.Y,
                TsvRecord.Header.PADDING,
                TsvRecord.Header.FONT);
    }
}
