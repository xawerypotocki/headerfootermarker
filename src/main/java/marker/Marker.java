package marker;

import marker.tsv.model.TsvPage;
import marker.tsv.processing.HeaderFooterMarker;
import marker.tsv.processing.TsvPageSplitter;
import marker.tsv.processing.TsvReader;
import marker.tsv.processing.TsvWriter;

import java.io.IOException;
import java.util.List;

public class Marker {
    public static final String INPUT_FILE_ENV = "input";
    public static final String OUTPUT_FILE_ENV = "output";

    // DEFAULT INPUT FILE NAME
    public static final String INPUT_TSV_FILE_NAME = "sds2912057926858607250.tsv";
    //private static final String INPUT_TSV_FILE_NAME = "sds5493674484985787039.tsv";
    //private static final String INPUT_TSV_FILE_NAME = "sds6470802517340212903.tsv";
    //private static final String INPUT_TSV_FILE_NAME = "sds6548799556800349124.tsv";
    //private static final String INPUT_TSV_FILE_NAME = "sds6979071286215649937.tsv";

    public static final String DEFAULT_INPUT_TSV_FILE = "./resources/testdata/" + INPUT_TSV_FILE_NAME;
    private static final String DEFAULT_OUTPUT_TSV_FILE = "./resources/output/" + System.currentTimeMillis() + INPUT_TSV_FILE_NAME;

    public static void main(String... arg) throws IOException {
        List<TsvPage> pages = new TsvPageSplitter().split(new TsvReader().read(getInputFile()));

        new HeaderFooterMarker().mark(pages);

        new TsvWriter().write(pages, getOutputFile());
    }

    private static String getInputFile() {
        return getFileFromEnv(INPUT_FILE_ENV, DEFAULT_INPUT_TSV_FILE);
    }

    private static String getOutputFile() {
        return getFileFromEnv(OUTPUT_FILE_ENV, DEFAULT_OUTPUT_TSV_FILE);
    }

    private static String getFileFromEnv(String envName, String defaultFile) {
        String fileName = System.getenv(envName);
        if (fileName == null)
            fileName = defaultFile;
        return fileName;
    }

}
